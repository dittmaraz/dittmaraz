---
title: "Article"
date: "2017-08-21"
description: ""
thumbnail: "img/placeholder.jpg" # Optional, thumbnail
lead: "Example lead - highlighted near the title"
comments: false # Enable/disable Disqus comments. Default value: true
authorbox: true # Enable authorbox for specific post
toc: true # Optional, enable Table of Contents for specific post
mathjax: true # Optional, enable MathJax for specific post
categories:
  - "Personal"
tags:
  - "Coding"
  - ""
menu: main # Optional, add page to a menu. Options: main, side, footer
---